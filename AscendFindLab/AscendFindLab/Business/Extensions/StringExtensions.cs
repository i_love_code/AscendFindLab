﻿using System;
using System.Text.RegularExpressions;

namespace AscendFindLab.Business.Extensions
{
    public static class StringExtensions
    {
        private static readonly Regex HtmlRegex = new Regex("<.*?>", RegexOptions.Compiled);
        private static readonly Regex EmptyHtmlRegex = new Regex("<[^>]*>\\s*<\\/[^>]*>", RegexOptions.Compiled);

        public static string FriendlyTruncate(this string source, int length)
        {
            source = source.RemoveHtmlTags();
            var sourceLength = source.Length;
            var sourceTruncated = source.Truncate(length);

            if (sourceLength <= length)
            {
                return sourceTruncated;
            }

            return $"{sourceTruncated}...";
        }

        public static string Truncate(this string source, int length)
        {
            return source.Substring(0, Math.Min(source.Length, length));
        }

        public static string RemoveHtmlTags(this string source)
        {
            return HtmlRegex.Replace(source, string.Empty);
        }

        public static string RemoveEmptyHtmlTags(this string source)
        {
            return EmptyHtmlRegex.Replace(source, string.Empty);
        }
    }
}