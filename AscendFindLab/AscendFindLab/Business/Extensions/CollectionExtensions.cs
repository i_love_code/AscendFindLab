﻿using System.Collections.Specialized;
using System.Web;

namespace AscendFindLab.Business.Extensions
{
    public static class CollectionExtensions
    {
        public static string ToQueryString(this NameValueCollection nameValueCollection)
        {
            var httpValueCollection = HttpUtility.ParseQueryString(string.Empty);

            foreach (var key in nameValueCollection.AllKeys)
            {
                if (!string.IsNullOrEmpty(nameValueCollection[key]))
                {
                    httpValueCollection[key] = nameValueCollection[key];
                }
            }

            return httpValueCollection.ToString();
        }
    }
}