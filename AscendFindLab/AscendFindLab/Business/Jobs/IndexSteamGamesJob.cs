﻿using System;
using EPiServer.Core;
using EPiServer.PlugIn;
using EPiServer.Scheduler;
using AscendFindLab.Steam.Services;
using EPiServer.Find.Framework;
using AscendFindLab.Business.Extensions;
using AscendFindLab.Steam.Models;
using System.Linq;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace AscendFindLab.Business.Jobs
{
    [ScheduledPlugIn(DisplayName = "Index SteamGame collection")]
    public class IndexSteamGamesJob : ScheduledJobBase
    {
        private int _batchSize = 50;
        private int _indexLimit = 9000;

        private bool _stopSignaled;

        public IndexSteamGamesJob()
        {
            IsStoppable = true;
        }

        /// <summary>
        /// Called when a user clicks on Stop for a manually started job, or when ASP.NET shuts down.
        /// </summary>
        public override void Stop()
        {
            _stopSignaled = true;
        }

        /// <summary>
        /// Called when a scheduled job executes
        /// </summary>
        /// <returns>A status message to be stored in the database log and visible from admin mode</returns>
        public override string Execute()
        {
            // Call OnStatusChanged to periodically notify progress of job for manually started jobs
            OnStatusChanged(String.Format("Starting execution of {0}", this.GetType()));

            // Add implementation

            // TODO: Switch to use SteamService
            var steamGames = SteamService.GetSteamGames();
            //var steamGameDataFileLocation = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"App_Data\SteamGamesData.json");
            //var steamGamesJson = File.ReadAllText(steamGameDataFileLocation);
            //var steamGames = JsonConvert.DeserializeObject<IEnumerable<SteamGame>>(steamGamesJson);

            OnStatusChanged(String.Format("Found {0} SteamGames...", steamGames.Count()));

            if (_stopSignaled)
            {
                return "Stopping job...";
            }

            // For our dataset, we want objects that have useful property values,
            // so we are filtering out those games without a HeaderImage
            // It's an OK indicator of the games that we have the additional details for
            var filteredSteamGames = steamGames.Where(x => !string.IsNullOrEmpty(x.HeaderImage));

            // If we were using a bigger index, we could index all of the SteamGame collection.
            // But since we are limited, we will only index 10000
            // Then we will batch index into groups of 50
            //var steamGameChunks = filteredSteamGames.Take(_indexLimit).Chunk(_batchSize);
            var steamGameChunks = filteredSteamGames.Chunk(_batchSize);

            OnStatusChanged(String.Format("Batch indexing {0} chunks...", steamGameChunks.Count()));

            if (_stopSignaled)
            {
                return "Stopping job...";
            }

            int chunkCount = 1;
            foreach (var chunk in steamGameChunks)
            {
                SearchClient.Instance.Index(chunk);

                OnStatusChanged(String.Format("Indexed {0} chunk of {1} chunks...", chunkCount++, steamGameChunks.Count()));

                if (_stopSignaled)
                {
                    return "Stopping job...";
                }
            }

            // For long running jobs periodically check if stop is signaled and if so stop execution
            if (_stopSignaled)
            {
                return "Stopping job...";
            }

            return "Completed indexing SteamGame collection";
        }
    }
}
