﻿using System.Web.Mvc;
using EPiServer.Web.Mvc;
using AscendFindLab.Models.Pages;
using AscendFindLab.Models.ViewModels;
using EPiServer.Find.Framework;
using AscendFindLab.Steam.Models;
using EPiServer.Find;
using EPiServer.Find.Api.Facets;
using AscendFindLab.Steam.Services;
using System.Linq;
using AscendFindLab.Models.Projections;
using AscendFindLab.Business.Extensions;

namespace AscendFindLab.Controllers
{
    [Route("Game/{action=Index}")]
    public class GameController : Controller
    {
        const int MAX_RELATED_GAMES = 9;

        [Route("Game/{gameId}")]
        public ActionResult Index(int gameId)
        {
            var games = SteamService.GetSteamGames();

            var game = games.FirstOrDefault(x => x.ApplicationId == gameId);

            var gameViewModel = new GamePageViewModel();

            gameViewModel.Game = game;

            #region Edit inside of this area

            // We should fill out our related games here

            #endregion

            return View(gameViewModel);
        }
    }
}