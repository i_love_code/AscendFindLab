﻿using System.Collections.Generic;
using AscendFindLab.Models.Projections;
using AscendFindLab.Steam.Models;

namespace AscendFindLab.Models.ViewModels
{
    public class GamePageViewModel
    {
        public SteamGame Game { get; set; }

        public List<SteamGameLite> RelatedGames { get; set; } = new List<SteamGameLite>();
    }
}