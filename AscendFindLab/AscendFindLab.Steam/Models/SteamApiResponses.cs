﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AscendFindLab.Steam.Models
{
    public class SteamApiResponses
    {
        public class SteamAppResponse
        {
            public int appid { get; set; }
            public string name { get; set; }
        }

        public class SteamAppDetailsResponse
        {
            public string type { get; set; }
            public string name { get; set; }
            public string required_age { get; set; }
            public bool is_free { get; set; }
            public string detailed_description { get; set; }
            public string short_description { get; set; }
            public string about_the_game { get; set; }
            public string website { get; set; }
            public IEnumerable<string> developers { get; set; }
            public IEnumerable<string> publishers { get; set; }
            public string header_image { get; set; }

            public IEnumerable<SteamAppDetailsAttribute> categories { get; set; }
            public IEnumerable<SteamAppDetailsAttribute> genres { get; set; }

            public string background { get; set; }
        }

        public class SteamAppDetailsAttribute
        {
            public int id { get; set; }
            public string description { get; set; }
        }

        public class SteamAppListResponse
        {
            public SteamAppListResponseWrapper2 applist { get; set; }
        }
        public class SteamAppListResponseWrapper2
        {
            public SteamAppListResponseWrapper3 apps { get; set; }
        }
        public class SteamAppListResponseWrapper3
        {
            public IEnumerable<SteamAppResponse> app { get; set; }
        }
    }
}
