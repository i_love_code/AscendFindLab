﻿using System.Collections.Generic;
using System.Linq;

namespace AscendFindLab.Steam.Models
{
    public class SteamGame : ISteamGame
    {
        public int ApplicationId { get; set; }

        public string Name { get; set; }

        public string Type { get; set; }
        public string RequiredAge { get; set; }
        public bool IsFree { get; set; }
        public string DetailedDescription { get; set; }
        public string ShortDescription { get; set; }
        public string AboutGame { get; set; }
        public string Website { get; set; }
        public IEnumerable<string> Developers { get; set; }
        public IEnumerable<string> Publishers { get; set; }
        public string HeaderImage { get; set; }
        public string BackgroundImage { get; set; }

        public bool HasDetails { get; set; }

        public IEnumerable<SteamGameAttribute> Categories { get; set; }
        public IEnumerable<SteamGameAttribute> Genres { get; set; }

        public IEnumerable<string> CategoryTerms
        {
            get
            {
                return Categories.Select(x => x.Description);
            }
        }
        public IEnumerable<string> GenreTerms
        {
            get
            {
                return Genres.Select(x => x.Description);
            }
        }
        
        public string CategoryAndGenreTermsJoined => string.Join(" ", CategoryTerms.Concat(GenreTerms));
    }

    public class SteamGameAttribute
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
